# Por que voce deveria saber sobre cloud economics?
### #aws #finops #economics #tco

Você é eternamente responsável pelo que gasta na cloud.

É isso, vlw flw

---

Beleza, mas por onde começar?

Bora começar o tema de finops!

FinOps, ou “Operações Financeiras em Nuvem” é a prática de trazer responsabilidade financeira ao modelo de gasto variável da nuvem, permitindo que equipes distribuídas façam trade-off entre velocidade, custo e qualidade.

Eu sempre gosto de dar o máximo de autonomia para os times, porém é necessário uma carga de responsabilidade em anexo. Falando sobre financeiro, os times são responsáveis pelos gastos financeiros em nuvem.

Para ajudar a segmentar os gastos pelo time existem várias estratégias como tags e segmentação de contas.

A responsabilidade do time de gerir os gastos não deve impactar ou servir de muleta para ter arquiteturas que não escalam ou não seguem boas práticas de desenvolvimento, mas sim trazer o senso de dono do que está sendo oferecido para o cliente final e coletando o retorno desse investimento através dos resultados.

No aspecto econômico da nuvem a AWS divide o tema em 2 principais áreas: Valor do empresarial  e gerenciamento financeiro da nuvem.

O valor empresarial é composto por todos os componentes que promovem o negócio para melhorar todos os aspectos de performance do negócio e experiência do cliente. Embora o valor empresarial inclua conceitos já conhecidos como custo total de propriedade, ou TCO, há muito mais com muitos outros componentes que afetam diretamente a empresa. Nesse ponto é importante definir o valor financeiro para organização quanto ao atingimento do objetivo da entrega.

Exemplo prático:

- Em um sistema de e-commerce notou-se uma baixa conversão em um dos fluxos de venda de um determinado segmento. Em pesquisas com o usuário observou que a usabilidade do fluxo não estava muito intuitiva e eram necessárias melhorias. As melhorias foram implementadas pelo time de desenvolvimento que decidiu incluir o Amazon CloudFront para entrega de conteúdo e isso traria um pequeno acréscimo nos custos financeiros da nuvem.
  O resultado dessa implementação foi um retorno de 10% de conversão sobre o mês anterior.
  O custo dessa melhoria de usabilidade e na infraestrutura se justificaram com os resultados obtidos.

O gerenciamento financeiro da nuvem é a outra parte dos aspectos econômicos. Essa área se concentra em ajudar os times a serem bem-sucedidos no gerenciamento da infraestrutura. Para isso a AWS oferece 2 poderosas ferramentas:

[AWS Billing and Cost Management](https://docs.aws.amazon.com/pt_br/awsaccountbilling/latest/aboutv2/billing-getting-started.html) foca em apresentar o que foi utilizado e os gastos financeiros consolidados
[AWS Budgets](https://aws.amazon.com/pt/aws-cost-management/aws-budgets/) ajuda em todo planejamento financeiro para o futuro e monitoramento com resposta através do Budget Actions

O [AWS Budget](https://aws.amazon.com/pt/aws-cost-management/aws-budgets/) ajuda a proteger as empresas do EDoS (Economic Denial of Service/Sustainability) que muitas vezes é negligenciado. (Prometo escrever sobre ele no futuro).

Para amarrar todas as pontas a AWS criou o [Cloud Value Framework](https://aws.amazon.com/pt/economics/) com o objetivo de mostrar a estrutura de valor que a nuvem oferece.

![https://dev-to-uploads.s3.amazonaws.com/uploads/articles/eq4mkepyzmx6x8qsbcfe.png](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/eq4mkepyzmx6x8qsbcfe.png)

Sugiro aos times empoderados financeiramente a olharem para esse [modelo de economia na nuvem](https://aws.amazon.com/pt/economics/) que certamente agregará bastante nas discussões sobre melhorias arquiteturais que impactam com o aumento de custos na nuvem.

Por último é sempre legal dar uma olhadinha na [calculadora AWS](https://calculator.aws/#/) para estimar o custo da arquitetura proposta.

Vlw Flw