**Resultados preliminares do meu projeto para o mestrado da UFABC**

## **Título do Projeto**

*Avaliação comparativa entre gRPC e REST com .NET Core*

## **Objetivo**

*Comparar duas formas de execução de API para definir qual seria a mais performática quanto a tempo de execução em milissegundos dentro de um contexto específico.*

## **Motivação e Desafios de Pesquisa**

*Com o avanço das tecnologias de sistemas distribuídos em computação em nuvem tem sido cada vez mais importante a utilização de padrões que ajudem com a performance de comunicação.*

*O REST foi criado em 2000 como uma interface de programação de aplicações e ganhou espaço e apreço dos desenvolvedores, porém, o cenário na qual o REST foi criado ainda não tinha ampla utilização da computação em nuvem como no cenário atual.*

*Sistemas distribuídos em diferentes tecnologias e arquiteturas computacionais têm demonstrado uma sobrecarga em alguns sistemas distribuídos complexos por limitações naturais de performance, com isso vejo a necessidade de buscar novas formas, arquiteturas e protocolos mais performáticos do que o padrão atual de REST para cenários complexos.*

*No gRPC as mensagens são serializadas em Protobuf, um formato de mensagem binária que serializa rapidamente no cliente e no servidor.*

*Uma mensagem gRPC é sempre menor do que uma mensagem JSON equivalente. Outro ponto importante é que o gRPC foi projetado para HTTP/2, uma revisão importante do HTTP que fornece benefícios significativos de desempenho.*

*A tabela a seguir fornece uma comparação dos recursos entre as APIs gRPC e REST.*

![Comparativos](https://raw.githubusercontent.com/brbernardo/gRPCvsREST/main/Image/comparativo.png)

Figura 1 - Tabela compartativa *entre as APIs gRPC e REST*

*Desta forma para atender ao objetivo dessa avaliação irei comparar o tempo de execução de uma aplicação utilizando o protocolo gRPC e a mesma aplicação usando o padrão REST.*

## **Métricas**

*Utilizarei média aritmética simples, representada por ∑ x i /n. Onde x i é o tempo de execução em milissegundos e o n será o número de iterações realizadas.*

## **Indicadores**

*Para apoiar a análise dos resultados estarei utilizando três indicadores que serão referenciados a seguir em todo experimento como Error, StdDev e Median. Onde:*

*Error: índice que calcula a metade do intervalo de confiança de 99,9%*

*StdDev: índice que calcula o desvio padrão de todas as medições*

*Median: índice que calcula o valor que separa a metade mais alta de todas as medições*

## **Carga de Trabalho**

*Os serviços podem ser executados de forma independente para análise dos resultados pertinentes ao propósito de cada um.*

*Para executar os projetos, em um prompt de comando, siga as instruções abaixo.*

*Para execução do API REST*

```bash
cd gRPCvsREST\RestAPI
dotnet run -c Releas
```

*Para execução do Serviço gRPC*

```bash
cd gRPCvsREST\GrpcService
dotnet run -c Releas
```

*Para avaliação comparativa dos protocolos usando o Benchmark*

```bash
cd gRPCvsREST\Client
dotnet run -c Release
```

## **Ferramentas, Plataformas, Software**

*Para comparação entre o desempenho do gRPC (HTTP/2 com Protobuf) e do REST (HTTP com JSON) criei duas aplicações utilizando a linguagem .NET Core, uma para cada protocolo.As aplicações estao disponíveis no repositorio do GitHub [https://github.com/brbernardo/gRPCvsREST](https://github.com/brbernardo/gRPCvsREST)*

*Para geração dos resultados foi utilizada a biblioteca de código aberto BenchmarkDotNet. A aplicações estão disponíveis no repositório do GitHub*

*[https://github.com/brbernardo/BenchmarkDotNet](https://github.com/brbernardo/BenchmarkDotNet)*

*Inclui uma cópia do [BenchmarkDotNet](https://github.com/brbernardo/BenchmarkDotNet) no repositório [gRPCvsREST](https://github.com/brbernardo/gRPCvsREST) para facilitar reproduções futuras.*

*Para execução do experimento utilizarei um servidor virtual Ubuntu hospedado na plataforma Digitalocean com 1 vCPU AMD e 1 gigabyte de memória ram.*

*Será necessário a instalação do servidor gRPC para execução do projeto. Os passos para a instalação estão descritos nesta URL https://docs.microsoft.com/pt-br/aspnet/core/tutorials/grpc/grpc-start?view=aspnetcore-3.1&tabs=visual-studio-code*

## **Parâmetros, Fatores e Níveis**

*Utilizaremos o parâmetro de milissegundos em todo o experimento*

*Os fatores relevantes a esse experimento são o consumo de memória ram e rom, taxa de carregamento do sistema e processos em execução*

*Níveis do servidor no momento do teste*

```bash
System load:  0.05
Usage of /:   10.2% of 24.06GB
Memory usage: 23%
Swap usage:   0%
Processes:    99
```

## **Descrição dos Experimentos**

*A execução de experimentos será através da execução dos métodos GrpcGetMenssage feito em gRPC e RestGetMessage feito em REST. É através da observação da aplicação de Benchmark com 100 e 200 iterações.*

*As métricas e os indicadores podem variar e nos resultados apresentarei os números consolidados ao final da quantidade de iterações.*

*Legenda dos resultados*

- *Method: método usado na comparação*
- *IterationCount: número de iterações*
- *Mean: média aritmética de todas as medições*
- *Error: metade do intervalo de confiança de 99,9%*
- *StdDev: desvio padrão de todas as medições*
- *Median: valor que separa a metade mais alta de todas as medições*
- *1 us: 1 microssegundo (0.000001 segundo)*

## **Resultados**

*Como os experimentos de avaliação de desempenho serão conduzidos. Haverá apenas um tipo de experimentos ou tipos diferentes? Tipos podem variar de acordo com os objetivos, como a duração, o tipo de carga de trabalho, etc.*

*A tabela a seguir exibe os resultados da execução do Benchmark.*

![resultados](https://raw.githubusercontent.com/brbernardo/gRPCvsREST/main/Image/metricas%2Bind.png)

Figura 2 - Tabela de resultados

*O gráfico a seguir evidencia o ganho de desempenho considerável ao usar o gRPC.*

![Grafico de resultados](https://raw.githubusercontent.com/brbernardo/gRPCvsREST/main/Image/resultados.png)

Figura 3 - Grafico de resultados

## **Considerações finais e análise**

*No experimento o gRPC se demonstrou um pouco mais performático, 3 vezes mais em comparação com o Rest, em termos de tempo de execução.*

*Foi observado um considerável aumento da complexidade de execução de carga de trabalho quando comparados. O gRPC possui uma complexidade maior por necessitar da criação do servidor de serviço.*

*Os níveis de utilização do servidor se manteve equivalente na execução dos projetos se desconsiderarmos o consumo do servidor do serviço gRPC.*
