![tumbnail](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/0ua7tvo0l1it4up5q10a.png)

# Estratégias de segmentação de contas
### #aws #terraform #cloudnative #devops

Na [última conversa](https://dev.to/bernardo/criando-e-segmentando-contas-no-aws-organization-a9f) conseguimos criar uma conta de gerenciamento e atrelar contas existentes debaixo dessa conta. Nesse post vamos evoluir um pouco mais a estratégia de organização das contas na Aws com o Organization.

Normalmente a arquitetura de solução segue o organograma organizacional definido na arquitetura corporativa. Então se temos uma área de compras, outra área de tecnologia, outra área de CRM, uma boa prática seria termos contas de gerência para cada unidade da organização.

Mas afinal qual seria o objetivo disso?

Vou utilizar a analogia do garçom que traz uma conta para uma mesa de 30 pessoas, ele precisa dividir a conta e verificar quem consumiu o que para que todos paguem pelo que consumiu. Na empresa com uma única conta pagadora não é diferente, o time de controle financeiro teria que todo mês fazer um trabalho de verificar a conta única e segmentar essa conta para os gestores responsáveis. Tem uma forma mais fácil de criar uma conta por área de custo ou unidade organizacional (seja como estiverem chamando a divisão de dinheiro entre áreas).

Para resolvermos isso temos o serviço de unidade organizacional no AWS Organization. Com isso podemos refletir toda a hierarquia organizacional de gestão de custos criando uma OU e informando qual será a conta acima dela através do parent_id.

```Terraform
resource "aws_organizations_organizational_unit" "financial" {
  name      = "financial_unit"
  parent_id = 1234567
}
```

Em algumas empresas há um terceiro nível de divisão que muda de acordo com o modelo organizacional e qual a literatura de referência. O Togaf chama de serviço de negócio, mas pode ser subárea, produto, centro de custo de coordenação, enfim, qualquer forma que o terceiro nível de granularidade de conta é conhecido como conta de identidade.

Nesse nível vamos ter políticas mais específicas para atender aquela necessidade do produto de forma assertiva.

Na AWS não há nível hierárquico de políticas, todas têm o mesmo peso então não podemos fazer hierarquias de políticas, mas podemos fazer hierarquia de contas para aplicar essas políticas por conta.

Belezinha, mas vamos aumentar um pouco a complexidade e não falar apenas dos custos.

Você já pegou um ambiente de homologação ou de desenvolvimento mesmo que parou por que alguém foi “testar algo rapidinho” imagina isso em cloud com as pessoas podendo aplicar e criar políticas novas.

Para resolver esse dilema temos o último nível de hierarquia de conta que é conhecido de recursos. Costumo usar esse nível para segmentar ambiente de desenvolvimento, homologação e produção.

Arquiteturalmente a hierarquia de contas ficaria assim.

![Arquitetura de contas distribuidas](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/7yu1uliwk5bqe87o33jg.PNG)

Belezinha, mas vamos pro código?

```terraform
#main.tf
resource "aws_organizations_account" "root" {
  name      = "my_organization.root"
  email     = "tech@organizations.org"
}

resource "aws_organizations_organizational_unit" "engenharia" {
  name      = "engenharia_OU"
  parent_id = my_organization.root.id
}

resource "aws_organizations_organizational_unit" "suporte" {
  name      = "suporte_SN"
  parent_id = engenharia_OU.id
}

resource "aws_organizations_organizational_unit" "Dev" {
  name      = "dev_ambiente"
  parent_id = suporte_SN.id
}

resource "aws_organizations_organizational_unit" "Homol" {
  name      = "hml_ambiente"
  parent_id = suporte_SN.id
}

resource "aws_organizations_organizational_unit" "Prod" {
  name      = "prd_ambiente"
  parent_id = suporte_SN.id
}
```
A primeira vez que vi uma estruturação de conta assim me assustei, isso aumenta muito a complexidade do dia a dia sim, mas por uma boa causa. Sem essa segmentação não seria possível uma governança de políticas estruturada.

Lembre-se sempre que não existe hierarquia nas políticas de segurança da cloud e essa é a melhor prática para um ambiente que precisa de governança de serviços e dados fortes.

Beleza, se não te “convenci” acompanhe a próxima conversa que vai ser sobre políticas na AWS.

\#vlwflw

 

