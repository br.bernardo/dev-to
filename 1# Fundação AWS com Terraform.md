![tumbnail](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/0ua7tvo0l1it4up5q10a.png)

# 1# Fundação AWS com Terraform
### #aws #terraform #cloudnative #devops

A ideia aqui e ter uma serie de boas práticas para quem está iniciando e quer estruturar do zero sua fundamentação cloud.

Se pudesse dar um conselho para quem está começando a migrar para nuvem ou criando uma aplicação Cloud Native nova sem dúvida seria estruture bem suas contas de providers!

Alguns acordos do que vamos utilizar aqui:

​	·    AWS, AWS e AWS

​	·    Terraform (https://www.terraform.io/)

Por que AWS?

​	·    Tenho mais experiencia

​	·    Simples

​	·    Tinha que escolher alguma para facilitar nos exemplos

Por que Terraform?

​	·    Tenho mais experiencia

​	·    Não confundir cloud native com aws native


Sobre o que é Cloud Native >> https://www.infoq.com/br/articles/cloud-native-panel/

Sobre o que é o IAC >> https://www.infoq.com/br/news/2020/02/honeycomb-infrastructure-as-code/

Quando comecei na AWS era tudo mato e não existia nenhum serviço global para gerenciar as contas, então era supernormal criar contas máster para tudo.

Alguém tocou no coração do PO para priorizar a tratativa dessa dor latente a todos os usuários AWS da época e foi criado o AWS Organizations.

![Texto  Descrição gerada automaticamente](https://i.pinimg.com/originals/0c/c8/d5/0cc8d54d38a1cee45ed9b6ea5be5efda.jpg)

No próximo vamos conversar sobre boas práticas, criação de conta, criação de subcontas.

\#vlwflw

