# Desenvolvimento, arquitetura e boas praticas
### #discuss #architecture #devops #programming

Eu sempre fui fã de desenvolvimento de boas arquiteturas, talvez por já ter sofrido muito na vida na sustentação de sistemas ruins.

Alguns decisões ruins poderiam ter sido evitadas se tivessem simplesmente "parado para planejar".

Normalmente esse profissional quando questionado começa com o checklist do [GoHorse](https://gohorseprocess.com.br/extreme-go-horse-xgh/):

[ ]  O prazo era apertado
[ ]  O sistema é muito legado
[ ]  Não estava claro a atividade
[ ]  Foi meu colega que fez
[ ]  Mas assim também funciona 🤣

Nesse momento eu gosto de lembrar de um principio do manifesto ágil que e esquecido em alguns casos.

> ["Contínua atenção à excelência técnica e bom design aumenta a agilidade."](https://agilemanifesto.org/iso/ptbr/principles.html)

Isso tem mudado com o surgimento do padrão Build & Run.

As equipes de desenvolvimento têm autoridade total sobre os serviços que criam, não apenas criando, mas também implantando e apoiando-os

![Build & Run](https://i.pinimg.com/originals/82/eb/c5/82ebc51304ebb2aae69b6962bfd16966.jpg)

Quando as equipes de desenvolvimento são responsáveis por construir um sistemas e suportá-lo em produção e também tentarem construir a plataforma de construção para executá-lo, a organização pode acabar com várias plataformas irreconciliáveis. Isso é desnecessário, caro de operar (se até possível) e leva tempo longe das equipes que deveriam se concentrar na entrega de recursos, não na plataforma em que são executados.

- A transferência entre as equipes de desenvolvimento e operações mata a velocidade e a agilidade da produção.
- Adicionar uma pessoa de operações em cada equipe de desenvolvedor é como você acaba com 10 plataformas irreconciliáveis.
- A lei de Conway diz que a arquitetura de software se parecerá com a estrutura da organização, portanto, se quisermos que a plataforma seja independente, as equipes de desenvolvimento de um aplicativo precisam ser separadas da equipe que executa a plataforma de produção.
- Há um limite para as capacidades que uma equipe pode ter. Devs são devs; eles podem estender seu conhecimento a um certo nível, mas eles não são Ops.
- Manter operações e desenvolvimento como disciplinas / equipes separadas não é sustentável no nativo da nuvem.
- Muita liberdade traz caos, mas muita padronização estrangula a inovação.

## **Portanto**

Crie equipes em que cada uma tenha sua própria capacidade de construir um sistema distribuído com microsserviços gerenciados por agendamento dinâmico. As equipes de construção-execução serão responsáveis por construir aplicativos distribuídos, implantá-los e, em seguida, dar suporte aos aplicativos depois de executados. Todas as equipes Build-Run usam o mesmo conjunto padronizado de serviços de plataforma e implantam em uma única plataforma unificada que executa todos os aplicativos para toda a empresa. Esta plataforma é da responsabilidade da Equipa da Plataforma, que a implementa e apoia.

- As equipes Build-Run não são equipes de DevOps no sentido tradicional de que os desenvolvedores e as pessoas de operações sentam-se juntos.
- No Agile, a equipe de desenvolvimento também inclui recursos de teste de software, mas os produtos são entregues a uma equipe de operações separada para serem entregues à produção.
- Na nuvem nativa, uma verdadeira equipe multifuncional deve ser capaz de construir sistemas distribuídos.
- A equipe da plataforma é um tipo específico de equipe Build-Run, pois cria, implanta, provisiona e oferece suporte à plataforma e infraestrutura nativas da nuvem, mas funciona separadamente das equipes de desenvolvimento de aplicativos.

## **Consequentemente**

Há uma forte separação de responsabilidades definidas: As equipes Build-Run lidam com os aplicativos. A Equipe da Plataforma é responsável por construir e manter a plataforma operacional. A Equipe da Plataforma não precisa fazer parte da empresa - nuvens públicas como Google / AWS / Azure, etc., com suas plataformas automatizadas, podem formar uma Equipe da Plataforma interna desnecessário. Se uma equipe de plataforma for designada, eles são geralmente globais, oferecendo suporte a todos os serviços / aplicativos; As equipes Build-Run são separadas e contam com a plataforma padronizada fornecida pela Equipe da Plataforma.

- As equipes têm um nível limitado de autonomia e capacidade de se concentrar em suas verdadeiras tarefas.
- Os desenvolvedores ainda têm liberdade para escolher os componentes a serem executados na plataforma padronizada, desde que sejam compatíveis.

Acredito que ["as melhores arquiteturas, requisitos e designs emergem de equipes auto-organizáveis"](https://agilemanifesto.org/iso/ptbr/principles.html). O que tenho visto e a negligencia desse principio em muitos sistemas e times.

O papel do gestor (depois de gerir pessoas) e provocar para que tenhamos uma vida saúdavel em todo ciclo de desenvolvimento de software, sinceramente não vejo como teriamos um ciclo saudavel sem pensarmos em melhores arquiteturas para o problema que queremos resolver.

Vou postar com mais frequência sobre temas correlacionados a arquitetura, orientação a eventos e como a orientação a eventos nos ajuda a construção de plataformas mais reativas, resilientes e tolerantes a falhas.