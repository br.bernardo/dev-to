![tumbnail](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/0ua7tvo0l1it4up5q10a.png)

# Gerenciando políticas do AWS Organizations
### #aws #terraform #cloudnative #devops

Beleza, passamos por uns 3 posts ate chegar aqui.  E aqui que todos os outros faz mais sentido 😁

A base das políticas do AWS Organizations são as políticas de controle de serviços (Service control policies), SCP, são aplicadas na conta e não existe hierarquia de SCP. Única ressalva e a respeito do Deny, caso algum serviço tenha uma SCP de deny esse serviço não poderá ser acessado mesmo que tenha uma allow explicita em alguma outra SCP.

Para construir uma políticas no Organizations com Terraform e bem simples, apenas precisa chamar o recurso e escrever o bloco content em json.

```terraform
resource "aws_organizations_policy" "engenharia" {

  content = <<CONTENT
{
  "Version": "2012-10-17",
  "Statement": {
    "Effect": "Allow",
    "Action": "*",
    "Resource": "*"
  }
}
CONTENT
}
```

Alguns exemplos de SCP [aqui](https://docs.aws.amazon.com/organizations/latest/userguide/orgs_manage_policies_scps_examples.html)

Um ponto aqui que acho importante e a sintaxe do content. Ela é composta por elemento, proposito e efeitos suportados.

Segue a tabelinha:

![sintaxe SCP](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/8vab3egazc3zlqbjyta6.PNG)

Tem alguma politicas que gosto de recomendar para quem está se aventurando aqui.

Não permitir que uma conta saia da organização.

```terraform
resource "aws_organizations_policy" "engenharia" {

  content = <<CONTENT
{
    "Version": "2012-10-17",
    "Statement": {
        "Effect": "Deny",
        "Action": "organizations:LeaveOrganization",
        "Resource": "*"
    }
}

CONTENT
}
```

Restringir serviços não globais em região especifica

```terraform
resource "aws_organizations_policy" "engenharia" {

  content = <<CONTENT
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Sid": "RestrictRegion",
          "Effect": "Deny",
          "NotAction": [
              "a4b:*",
              "budgets:*",
              "ce:*",
              "chime:*",
              "cloudfront:*",
              "cur:*",
              "globalaccelerator:*",
              "health:*",
              "iam:*",
              "importexport:*",
              "mobileanalytics:*",
              "organizations:*",
              "route53:*",
              "route53domains:*"
              "shield:*",
              "support:*",
              "trustedadvisor:*",
              "waf:*",
              "wellarchitected:*"
          ],
          "Resource": "*",
          "Condition": {
              "StringNotEquals": {
                  "aws:RequestedRegion": [
                      "sa-east-1"

							]
          }
      }
  }
}
CONTENT
}
```

Negar possibilidade de interromper o CloudWatch

```terraform
resource "aws_organizations_policy" "engenharia" {

  content = <<CONTENT
{
 "Version": "2012-10-17",
    "Statement": {
        "Effect": "Deny",
        "Action": "organizations:LeaveOrganization",
        "Resource": "*"
  }
}
CONTENT
}
```

\#vlwflw